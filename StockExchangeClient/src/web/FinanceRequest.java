package web;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import data.*;

public class FinanceRequest {
	private boolean mirror;
	private LocalDataStore dS;
	public FinanceRequest(){
		mirror = false;
		dS = new LocalDataStore();
	}
	public void updateAllStocks(){
		Stock[] S = new Stock[256];
		String[][] results = new String[256][7];
		String[] r = new String[256];
		try {
			URL fileLink;
			if (mirror){
				fileLink = new URL("http://www.oldbridgebuffet.com/results.csv");
			} else {
				fileLink = new URL("http://download.finance.yahoo.com/d/quotes.csv?s=PCLN,AMZN,GOOGL,GOOG,ISRG,AZO,MTD,CMG,REGN,EQIX,BLK,CHTR,SHW,BCR,ULTA,LMT,TDG,ESS,NOC,BIIB,ORLY,ADS,MHK,HUM,AVGO,ROP,MLM,AGN,COO,GS,PSA,MMM,GD,FDX,BDX,AVB,WHR,ANTM,BA,HSIC,STZ,WAT,ILMN,COST,UNH,TMO,GWW,IDXX,LLL,AYI,PXD,MCK,CI,SNA,RTN,NFLX,ROK,CMI,AMG,PH,MTB,LRCX,AMGN,HD,SPG,AAPL,IBM,MCD,FB,AET,EXPE,WLTW,APD,CB,SYK,NVDA,SPGI,ITW,LH,NEE,ADBE,INTU,SWK,IFF,EFX,AAP,CLX,PX,ECL,WYNN,HON,AMT,AON,SJM,FFIV,KMB,INCY,JNJ,CXO,VRTX,CTAS,VMC,TRV,ACN,FISV,DE,NSC,FRT,MA,AMP,UTX,BXP,PM,PNC,MCO,CME,DLR,MON,EW,PEP,SRE,CELG,HSY,UHS,EA,ALB,HRS,ADSK,RCL,HAR,UNP,DTE,URI,DGX,PPG,MAR,DIS,COL,SWKS,UPS,DNB,XEC,PRU,PVH,HAS,CAT,MKC,KLAC,CVX,CCI,ADP,WYN,SLG,MAA,VAR,ALXN,AIZ,TWX,TAP,KSU,V,EL,GPC,GPN,ABC,DPS,VNO,KHC,VRSN,WDC,RHT,EOG,DRI,CRM,MJN,IR,TIF,DLPH,PNW,NTRS,ALK,PG,ALL,CBOE,FIS,DUK,DHR,MDT,JBHT,AVY,MSI,OMC,SRCL,HCA,DOV,ED,MCHP,TSO,JPM,CTXS,STT,ADI,WBA,EIX,TXN,D,VRSK,EMN,UAL,XOM,DLTR,LYB,LOW,DD,LLY,ETR,TEL,WMT,ALLE,QRVO,AWK,CVS,AXP,DG,ETN,COF,MMC,CL,PSX,TMK,FMC,AFL,TJX,EXR,MO,CNC,APH,CAH,PRGO,YUM,WM,HCN,K,AEP,TROW,CINF,MSFT,SLB,RL,CMA,PCG,SCG,NDAQ,CHRW,DVA,CTSH&f=nsl1opc1p2&e=.csv");
			}
			Scanner scanner = new Scanner(fileLink.openStream());
			for (int i=0;i<256;i++){
				r[i] = scanner.nextLine();
				String m = r[i].replace("\""," ");
				m = r[i].replaceAll("%","");
				m = r[i].replaceAll(", "," ");
				String[] n = m.split(",");
				for (int j=0;j<7;j++){
					results[i][j] = n[j];
				}
			}
			scanner.close();
			for (int i=0;i<256;i++){
				S[i] = new Stock(results[i][1].replaceAll("\"", ""),i);
				S[i].setName(results[i][0].replaceAll("\"", ""));
				S[i].setPrice(Double.parseDouble(results[i][2].replaceAll("\"", "")));
				S[i].setChange(Double.parseDouble(results[i][5].replaceAll("\"", "")));
				S[i].setPercentChange(Double.parseDouble(results[i][6].replaceAll("\"", "").replaceAll("%", "")));
			}
			dS.updateStocks(S);
		} catch (Exception E){
			E.printStackTrace();
			System.out.println("Error in retrieving stock data, will try mirror.");
			dS.getDisplay().showNotification("Error in retrieving stock data, will try mirror.");
			mirror = true;
			updateAllStocks();
		}
	}
	public ArrayList<Double> getHistorical(Stock A, int days){
		ArrayList<Double> results = new ArrayList<Double>();
		try {
			URL fileLink = new URL("http://www.google.com/finance/historical?q=NASDAQ%3A"+A.getSymbol()+"&ei=NSE_WcnMMoPEeIrWmNgL&output=csv");
			Scanner scanner = new Scanner(fileLink.openStream());
			double low = Double.MAX_VALUE, high = 0;
			scanner.nextLine();
			for (int i=0;i<250;i++){
				String r = scanner.nextLine();
				r.replace("\""," ");
				r.replaceAll("%","");
				r.replaceAll(", "," ");
				String[] n = r.split(",");
				Double d = Double.parseDouble(n[4]);
				results.add(d);
				if (d.doubleValue()<low){
					low = d.doubleValue();
				}
				if (d.doubleValue()>high){
					high = d.doubleValue();
				}
			}
			scanner.close();
			System.out.println("Stock history for "+A.getSymbol()+" received.");
			for (int i=0;i<results.size()/2;i++){
				Double d = results.get(i);
				results.set(i, results.get(results.size()-i-1));
				results.set(results.size()-i-1, d);
			}
			System.out.println(results.size());
			results.subList(0, 250-days).clear();
		} catch (Exception E){
			System.out.println("Error in retrieving stock graphs, please try again later.");
			dS.getDisplay().showNotification("Error in retrieving stock graphs, please try again later.");
		}
		return results;
	}
}
