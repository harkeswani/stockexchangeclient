package update;
import web.FinanceRequest;
import data.*;
import graphics.*;
public class GUIUpdate extends Thread{
	Display d;
	LocalDataStore dS;
	FinanceRequest fR;
	public GUIUpdate(Display D){
		d = D;
		dS = new LocalDataStore();
		fR = new FinanceRequest();
		run();
	}
	public void run(){
		while (true){
			for (int i=0;i<60;i++){
				try {
					sleep(1000);
					d.updateBuySellTotal();
					if (i%2==0){
						fR.updateAllStocks();
						d.refreshStockTable();
					}
				} catch (Exception E){
					System.out.println("GUI Update interrupted.");
					run();
				}
			}
		}
	}
}
