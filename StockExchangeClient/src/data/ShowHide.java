package data;
import javax.swing.*;
public class ShowHide extends Thread{
	JPanel C;
	int T;
	public ShowHide(JPanel A, int T){
		C = A;
		run();
	}
	public void run(){
		try {
			sleep(T); // Sleep in milliseconds
			C.setVisible(false);
			LocalDataStore dS = new LocalDataStore();
			C.revalidate();
		} catch (Exception E){
			if (C.getComponents()[0] instanceof JLabel){
				System.out.println(((JLabel)C.getComponents()[0]).getText()); // Print error if failed
				E.printStackTrace();
			}
		}
	}
}
