package data;

public class StockOwned {
	private Stock stock;
	private int quantity;
	public StockOwned(Stock S){
		stock = S;
		quantity = 0;
	}
	public StockOwned(Stock S, int Q){
		stock = S;
		quantity = Q;
	}
	public void addStocks(int Q){
		quantity+=Q;
	}
	public void removeStocks(int Q){
		if (quantity>=Q){
			quantity-=Q;
		}
	}
	public Stock getStock(){
		return stock;
	}
	public int getQuantity(){
		return quantity;
	}
}
