package data;

import graphics.Display;

import java.util.ArrayList;
import java.util.Date;

import save.SaveToDisk;

public class StockAccount extends Account{
	private ArrayList<StockOwned> stocksOwned;
	private LocalDataStore dS;
	public StockAccount(){
		super();
		stocksOwned = new ArrayList<StockOwned>();
		dS = new LocalDataStore();
	}
	public void addStock(Stock A, int Q){
		if (getIndexOfStockOwned(A) == -1){
			stocksOwned.add(new StockOwned(A,Q));
		} else {
			stocksOwned.get(getIndexOfStockOwned(A)).addStocks(Q);
		}
	}
	public void removeStock(Stock A, int Q){
		if (getIndexOfStockOwned(A) != -1){
			stocksOwned.get(getIndexOfStockOwned(A)).removeStocks(Q);
			if (stocksOwned.get(getIndexOfStockOwned(A)).getQuantity()<1){
				stocksOwned.remove(stocksOwned.get(getIndexOfStockOwned(A)));
			} else {
				dS.getDisplay().showNotification("Not enough stocks to complete sale!");
			}
		}
	}
	public void buyStock(Stock A, int Q){
		double total = A.getPrice()*Q;
		if (getBalance()>=total){
			removeBalance(total);
			addStock(A,Q);
			SaveToDisk dK = new SaveToDisk();
			Date d = new Date();
			dK.saveToLog(Q+" Stocks of "+A.getSymbol()+" bought for $"+total+" at "+d.toString());
		} else {
			dS.getDisplay().showNotification("Funds too low ($"+getBalance()+") to buy $"+total+" worth of stock.");
		}
	}
	public void sellStock(Stock A, int Q){
		double total = A.getPrice()*Q;
		if (getIndexOfStockOwned(A)!=-1){
			if (stocksOwned.get(getIndexOfStockOwned(A)).getQuantity()>=Q){
				removeStock(A,Q);
				addBalance(total);
				SaveToDisk dK = new SaveToDisk();
				Date d = new Date();
				dK.saveToLog(Q+" Stocks of "+A.getSymbol()+" sold for $"+total+" at "+d.toString());
			}	
		} else {
			dS.getDisplay().showNotification("Not enough stocks to complete sale!");
		}
	}
	public int getIndexOfStockOwned(Stock A){
		for (int i=0;i<stocksOwned.size();i++){
			if (stocksOwned.get(i).getStock().getID() == A.getID()){
				return i;
			}
		}
		return -1;
	}
	public ArrayList<StockOwned> getStocksOwned(){
		return stocksOwned;
	}
	public double genTotalValue(){
		double result = getBalance();
		for (StockOwned A : stocksOwned){
			result+=(A.getStock().getPrice()*A.getQuantity());
		}
		return result;
	}
	public String toString(){
		String result = "Liquid: $"+getBalance()+"\n";
		for (StockOwned A : stocksOwned){
			result+=A.getStock().getID()+"\t"+A.getStock().getSymbol()+"\t"+"$"+A.getStock().getPrice()+"\t"+A.getQuantity()+" Owned"+"\n";
		}
		result+="Total Value: $"+genTotalValue();
		return result;
	}
}
