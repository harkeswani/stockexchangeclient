package data;

public class Stock {
	private String symbol;
	private String name;
	private double price;
	private double change;
	private double percentChange;
	private int id;
	public Stock(String S, int I){
		symbol = S;
		name = "";
		price = 0;
		change = 0;
		percentChange = 0;
		id = I;
	}
	public void setName(String A){
		name = A;
	}
	public void setPrice(double A){
		price = A;
	}
	public void setChange(double A){
		change = A;
	}
	public void setPercentChange(double A){
		percentChange = A;
	}
	public int getID(){
		return id;
	}
	public String getSymbol(){
		return symbol;
	}
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}
	public double getChange(){
		return change;
	}
	public double getPercentChange(){
		return percentChange;
	}
	public String toString(){
		String result = symbol;
		result+="\n"+name;
		result+="\n"+"$"+roundMoney(price);
		result+="\n"+"$"+roundMoney(change);
		result+="\n"+percentChange+"%";
		return result;
	}
	public double roundMoney(double A){
		return (Math.round(A*100))/100.0;
	}
}
