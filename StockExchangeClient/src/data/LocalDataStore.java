package data;
import graphics.*;
import web.*;
import data.*;

public class LocalDataStore {
	private static Stock[] stocks;
	private static String username;
	private static String password;
	private static BankAccount bankAccount;
	private static StockAccount stockAccount;
	private static Display d;
	public LocalDataStore(Display D){
		d = D;
	}
	public LocalDataStore(){
		
	}
	public Display getDisplay(){
		return d;
	}
	public double getTotalValue(){
		return bankAccount.getBalance()+stockAccount.genTotalValue();
	}
	public void logon(String A, String B){
		username = A;
		password = B;
	}
	public Stock getStockInfo(Stock A){
		return A;
	}
	public void updateStocks(Stock[] S){
		stocks = S;
	}
	public void initBankAccount(int A){
		bankAccount = new BankAccount(A);
	}
	public void initStockAccount(){
		stockAccount = new StockAccount();
	}
	public Stock[] getStockArray(){
		return stocks;
	}
	public Stock getStock(int A){
		return stocks[A];
	}
	public StockAccount getStockAccount(){
		return stockAccount;
	}
	public BankAccount getBankAccount(){
		return bankAccount;
	}
	public void initStocks(){
		stocks = new Stock[256];
		String [] s = new String[]{"PCLN","AMZN","GOOGL","GOOG","ISRG","AZO","MTD","CMG","REGN","EQIX","BLK","CHTR","SHW","BCR","ULTA","LMT","TDG","NOC","ESS","ORLY","BIIB","ADS","AVGO","MHK","HUM","ROP","MLM","AGN","GS","COO","PSA","MMM","GD","FDX","AVB","BDX","BA","ANTM","WHR","HSIC","STZ","COST","WAT","UNH","ILMN","TMO","GWW","IDXX","LLL","PXD","NFLX","RTN","AYI","MCK","CI","SNA","ROK","PH","MTB","CMI","LRCX","SPG","HD","AMGN","AAPL","AMG","FB","IBM","MCD","AET","WLTW","NVDA","EXPE","APD","ADBE","SYK","CB","ITW","SPGI","NEE","INTU","LH","IFF","SWK","EFX","CLX","AAP","HON","AMT","PX","ECL","AON","INCY","CXO","KMB","FFIV","SJM","JNJ","WYNN","VMC","CTAS","FISV","NSC","FRT","ACN","TRV","DE","MA","UTX","AMP","BXP","VRTX","PM","PNC","IT","ZBH","PEP","DLR","CME","ARE","MON","MCO","SRE","HSY","CELG","UHS","EW","ADSK","EA","ALB","UNP","URI","HRS","RCL","DTE","DIS","DGX","XEC","COL","UPS","MAR","PPG","SWKS","PVH","CAT","PRU","KLAC","HAS","CVX","MKC","CCI","ADP","SLG","MAA","WYN","TWX","VAR","AIZ","ALXN","KSU","TAP","V","EL","DPS","GPC","KHC","VNO","GPN","WDC","ABC","CRM","VRSN","EOG","RHT","MJN","IR","NTRS","PNW","DRI","PG","CBOE","DLPH","ALL","JBHT","TIF","ALK","DUK","FIS","MDT","DHR","ADI","AVY","TSO","JPM","OMC","DOV","MSI","MCHP","ED","CTXS","HCA","TXN","STT","SRCL","XOM","VRSK","D","EIX","LYB","WBA","EMN","LOW","UAL","TEL","QRVO","ALLE","LLY","DXC","COF","ETR","DLTR","WMT","AWK","ETN","DD","MMC","EXR","AXP","PSX","CVS","CL","TMK","AFL","MO","FMC","APH","TJX","SNPS","CAH","CNC","DG","RJF","HCN","YUM","WM","K","AEP","MSFT","TROW","CINF"};
		for (int i=0;i<s.length;i++){
			stocks[i] = new Stock(s[i],i);
		}
	}
}
