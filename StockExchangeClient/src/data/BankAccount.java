package data;

public class BankAccount extends Account{
	public BankAccount(int A){
		super(A);
	}
	public void transferStockAccount(StockAccount A, int B){
		if (getBalance()>=B){
			removeBalance(B);
		} else {
			LocalDataStore dS = new LocalDataStore();
			dS.getDisplay().showNotification("Not enough balance to transfer.");
		}
	}
}
