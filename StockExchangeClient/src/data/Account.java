package data;

import java.util.Date;

import save.SaveToDisk;

public abstract class Account {
	private double balance;
	public Account(){
		balance = 0;
	}
	public Account(double A){
		balance = A;
	}
	public void addBalance(double A){
		balance+=A;
	}
	public void removeBalance(double A){
		if (balance>=A){
			balance-=A;
		}
	}
	public void transferBalance(Account A, double B){
		if (balance>=B){
			balance-=B;
			A.addBalance(B);
			SaveToDisk dK = new SaveToDisk();
			Date d = new Date();
			dK.saveToLog("$"+B+" transferred to Account at "+d.toString());
		} else {
			LocalDataStore dS = new LocalDataStore();
			dS.getDisplay().showNotification("Not enough funds to transfer $"+B+".");
		}
	}
	public void setBalance(double A){
		balance = A;
	}
	public double getBalance(){
		return balance;
	}
}
