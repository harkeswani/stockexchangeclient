package graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Random;

import data.*;
import save.SaveToDisk;
import web.*;

import javax.net.ssl.SSLEngineResult.Status;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import data.LocalDataStore;
public class Display extends JFrame implements ActionListener {
	private int scaleSizeX;
	private int scaleSizeY;
	private JLayeredPane body;
	private JPanel userPanel;
	private JTable stockTable;
	private JScrollPane scroll;
	private LocalDataStore dS;
	private JPanel notification;
	private boolean notificationVisible;
	private JPanel buySell;
	private JPanel portfolio;
	private boolean portfolioInit;
	private JSpinner bSID;
	private JSpinner bSQu;
	private JLabel bSTo;
	private JTextField userName;
	private JTextField password;
	private JButton login;
	private boolean chartVisible;
	private JSpinner chartID;
	private JSpinner dayCount;
	private JSpinner transferBank;
	private JSpinner transferStock;
	private JPanel log;
	private boolean logVisible;
	private JPanel graphMain;
	private JSpinner gMID;
	private JSpinner gMDY;
	private JPanel graphA;
	private JSpinner gAID;
	private JSpinner gADY;
	private JPanel graphB;
	private JSpinner gBID;
	private JSpinner gBDY;
	public Display(int X, int Y) {
		setTitle("Stock Exchange");
		setSize(X, Y);
		scaleSizeX = X / 16;
		scaleSizeY = Y / 12;
		setUndecorated(true);
		setVisible(true);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dS = new LocalDataStore();
		dS.initStocks();
		body = new JLayeredPane();
		body.setSize(X, Y);
		add(body);
		body.setLocation(0, 0);
		body.setVisible(true);
		body.setLayout(null);
		JPanel background = new JPanel();
		background.setSize(X, Y);
		body.add(background);
		background.setLocation(0, 0);
		background.setBackground(Color.YELLOW);
		background.setVisible(true);
		initUserPanel();
		refreshStockTable();
		refreshBuySell();
		refreshPortfolio();
		transferPanel();
		initGraphMain();
		showNotification(JLabelMultiLine("Stock Exchange! 1.0.1 Beta"+"\n"+" Made by Harshal Keswani"+"\n"+"Multiplayer coming soon!"));
	}
	public void actionPerformed(ActionEvent E) {
		FinanceRequest fR = new FinanceRequest();
		fR.updateAllStocks();
		SaveToDisk disk = new SaveToDisk();
		String command = E.getActionCommand();
		int route = Integer.parseInt(command.substring(0, 1));
		switch (route){
		case 0:
			if (command.substring(1, command.length()).equals("NO")){
				hideNotification();
			} else if (command.substring(1, command.length()).equals("LO")){
				hideLog();
			}
			break;
		case 1:
			dS.getStockAccount().buyStock(dS.getStock((Integer)bSID.getValue()), (Integer)bSQu.getValue());
			bSQu.setValue(0);
			refreshPortfolio();
			disk = new SaveToDisk();
			disk.savePortfolio();
			break;
		case 2:
			dS.getStockAccount().sellStock(dS.getStock((Integer)bSID.getValue()), (Integer)bSQu.getValue());
			bSQu.setValue(0);
			refreshPortfolio();
			disk = new SaveToDisk();
			disk.savePortfolio();
			break;
		case 3:
			login(userName.getText(),password.getText());
			disk = new SaveToDisk();
			disk.savePortfolio();
			break;
		case 4:
			disk = new SaveToDisk();
			disk.savePortfolio();
			break;
		case 5:
			disk = new SaveToDisk();
			disk.loadPortfolio();
			break;
		case 6:
			viewLog();
			break;
		case 8:
			dS.getBankAccount().transferBalance(dS.getStockAccount(), (int)transferBank.getValue());
			dS.getStockAccount().transferBalance(dS.getBankAccount(), (int)transferStock.getValue());
			transferBank.setValue(0);
			transferStock.setValue(0);
			refreshPortfolio();
			disk = new SaveToDisk();
			disk.savePortfolio();
			break;
		case 9:
			refreshGraphMain(dS.getStock((int)chartID.getValue()),(int)dayCount.getValue());
			break;
		}
	}
	public void load(){
		SaveToDisk disk = new SaveToDisk();
		disk.loadPortfolio();
	}
	public void initUserPanel(){
		userPanel = new JPanel();
		userPanel.setSize(scaleSizeX * 16, scaleSizeY);
		body.add(userPanel);
		userPanel.setLocation(0, 0);
		userPanel.setLayout(null);
		body.setLayer(userPanel, 1);
		userPanel.setBackground(new Color(150, 100, 50));
		userPanel.setVisible(true);
		userName = new JTextField("Username");
		userName.setSize(new Dimension(scaleSizeX*2,scaleSizeY/2));
		userPanel.add(userName);
		userName.setLocation(scaleSizeX*10, scaleSizeY/4);
		userName.setFont(new Font("Arial",Font.ITALIC,scaleSizeY/3));
		userName.setVisible(true);
		password = new JTextField("Password");
		password.setSize(new Dimension(scaleSizeX*2,scaleSizeY/2));
		userPanel.add(password);
		password.setLocation(scaleSizeX*49/4, scaleSizeY/4);
		password.setFont(new Font("Arial",Font.ITALIC,scaleSizeY/3));
		password.setVisible(true);
		login = new JButton("Login");
		login.setSize(scaleSizeX,scaleSizeY/2);
		userPanel.add(login);
		login.setLocation(scaleSizeX*29/2,scaleSizeY/4);
		login.setBackground(new Color(50,250,100));
		login.setVisible(true);
		login.addActionListener(this);
		login.setActionCommand("3");
		DataGet();
	}
	public void login(String A, String B) {
		dS.logon(A, B);
	}
	public void setup() {
		LocalDataStore dS = new LocalDataStore();
		dS.initStocks();
		FinanceRequest fR = new FinanceRequest();
		fR.updateAllStocks();
		DataGet();
	}
	public void refreshStockTable() {
		FinanceRequest fR = new FinanceRequest();
		fR.updateAllStocks();
		String[] col = new String[] {
			"ID",
			"Symbol",
			"Name",
			"Price",
			"Change",
			"Percent Change",
			"Buy",
			"Sell"
		};
		Object[][] data = new Object[256][6];
		Stock[] stocks = dS.getStockArray();
		for (int i = 0; i < stocks.length; i++) {
			data[i][0] = "" + stocks[i].getID();
			data[i][1] = stocks[i].getSymbol();
			data[i][2] = stocks[i].getName();
			data[i][3] = "$" + stocks[i].getPrice();
			data[i][4] = "$" + stocks[i].getChange();
			data[i][5] = stocks[i].getPercentChange() + "%";
		}
		DefaultTableModel model = new DefaultTableModel(data, col);
		stockTable = new JTable(model);
		stockTable.setSize(scaleSizeX * 12, scaleSizeY * 6);
		stockTable.setRowHeight(scaleSizeY / 4);
		stockTable.setBackground(new Color(63,63,63));
		stockTable.getColumnModel().getColumn(2).setMinWidth(scaleSizeX * 4);
		stockTable.setFont(new Font("Arial", Font.PLAIN, 20));
		stockTable.setForeground(Color.WHITE);
		scroll = new JScrollPane(stockTable);
		stockTable.setVisible(true);
		for (int i = 0; i < 256; i++) {
			JButton buy = new JButton("Buy");
			buy.setSize(scaleSizeX, scaleSizeY / 4);
			scroll.add(buy);
			buy.setLocation(scaleSizeX * 9 + scaleSizeX * 3 / 4, 20 + 20 * i);
			buy.setVisible(true);
		}
		scroll.setSize(scaleSizeX * 12, scaleSizeY * 6);
		body.add(scroll);
		body.setLayer(scroll, 1);
		scroll.setLocation(0, scaleSizeY);
	}
	public void DataGet(){
		JButton save = new JButton("Save");
		save.setSize(scaleSizeX/2,scaleSizeY*3/4);
		userPanel.add(save);
		save.setLocation(scaleSizeX*8,scaleSizeY/8);
		save.setMargin(new Insets(0, 0, 0, 0));
		save.setVisible(true);
		save.setFont(new Font("Arial",Font.BOLD,scaleSizeX/6));
		save.addActionListener(this);
		save.setActionCommand("4");
		JButton load = new JButton("Load");
		load.setSize(scaleSizeX/2,scaleSizeY*3/4);
		userPanel.add(load);
		load.setLocation(scaleSizeX*17/2,scaleSizeY/8);
		load.setMargin(new Insets(0, 0, 0, 0));
		load.setVisible(true);
		load.setFont(new Font("Arial",Font.BOLD,scaleSizeX/6));
		load.addActionListener(this);
		load.setActionCommand("5");
		JButton log = new JButton("Log");
		log.setSize(scaleSizeX/2,scaleSizeY*3/4);
		userPanel.add(log);
		log.setLocation(scaleSizeX*9,scaleSizeY/8);
		log.setMargin(new Insets(0, 0, 0, 0));
		log.setVisible(true);
		log.setFont(new Font("Arial",Font.BOLD,scaleSizeX/6));
		log.addActionListener(this);
		log.setActionCommand("6");
	}
	public void initGraphMain(){
		graphMain = new JPanel();
		graphMain.setSize(scaleSizeX*6-scaleSizeX/8,scaleSizeY*4-scaleSizeY/8);
		body.add(graphMain);
		graphMain.setLocation(scaleSizeX*3+scaleSizeX/16, scaleSizeY*4+scaleSizeY/16);
		graphMain.setLayout(null);
		graphMain.setBackground(new Color(200,50,100));
		graphMain.setVisible(true);
		JLabel t = new JLabel("Select Graph to Display");
		t.setSize(scaleSizeX * 3, scaleSizeY / 2);
		graphMain.add(t);
		t.setLocation(scaleSizeX/3*2, scaleSizeY / 8);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		JLabel tID = new JLabel("Stock ID (0-255)");
		tID.setSize(scaleSizeX*2,scaleSizeY/2);
		graphMain.add(tID);
		tID.setLocation(scaleSizeX/4,scaleSizeY/2);
		tID.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		tID.setForeground(Color.WHITE);
		tID.setVisible(true);
		SpinnerModel m1 = new SpinnerNumberModel(0,0,255,1);
		gMID = new JSpinner(m1);
		gMID.setSize(scaleSizeX,scaleSizeY/4);
		graphMain.add(gMID);
		gMID.setLocation(scaleSizeX*5/2,scaleSizeY/3*2);
		gMID.setVisible(true);
		JLabel dayT = new JLabel("# of Trading Days (1-250)");
		dayT.setSize(scaleSizeX*2,scaleSizeY);
		graphMain.add(dayT);
		dayT.setLocation(scaleSizeX/4,scaleSizeY*2/3);
		dayT.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		dayT.setForeground(Color.WHITE);
		dayT.setVisible(true);
		SpinnerModel m2 = new SpinnerNumberModel(30,1,250,1);
		gMDY = new JSpinner(m2);
		gMDY.setSize(scaleSizeX,scaleSizeY/4);
		graphMain.add(gMDY);
		gMDY.setLocation(scaleSizeX*5/2,scaleSizeY);
		gMDY.setVisible(true);
	}
	public void refreshGraphMain(Stock A, int days){
		FinanceRequest fR = new FinanceRequest();
		Graph graph = new Graph(fR.getHistorical(A,days));
		graph.setPreferredSize(graphMain.getSize());
		graphMain.add(graph);
		graph.setLocation(0,0);
		graph.setVisible(true);
		graph.revalidate();
	}
	public void viewLog(){
		if (logVisible) {
			body.remove(log);
			body.revalidate();
		}
		logVisible = true;
		log = new JPanel();
		log.setSize(scaleSizeX * 8, scaleSizeY * 6);
		body.add(log);
		body.setLayer(log, 3);
		log.setBackground(new Color(200, 100, 100));
		log.setLocation(scaleSizeX * 4, scaleSizeY * 3);
		log.setVisible(true);
		log.setLayout(null);
		JLabel t = new JLabel("Log:");
		t.setSize(scaleSizeX * 7, scaleSizeY);
		log.add(t);
		t.setBackground(new Color(100, 200, 200));
		t.setLocation(scaleSizeX, 0);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX*2/3));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		SaveToDisk dK = new SaveToDisk();
		JLabel e = new JLabel(JLabelMultiLine(dK.getLog()));
		e.setSize(scaleSizeX * 8, scaleSizeY * 5);
		log.add(e);
		e.setBackground(new Color(100, 200, 100));
		e.setLocation(0, scaleSizeY);
		e.setFont(new Font("Arial", Font.BOLD, scaleSizeX / 4));
		e.setVisible(true);
		JButton c = new JButton("X"); // Close notification window
		c.setSize(scaleSizeX, scaleSizeY);
		log.add(c);
		c.setLocation(scaleSizeX * 7, 0);
		c.setBackground(Color.GREEN);
		c.setFont(new Font("Arial", Font.PLAIN, scaleSizeX / 2));
		c.setForeground(Color.WHITE);
		c.setVisible(true);
		c.addActionListener(this);
		c.setActionCommand(0 + "LO");
	}
	public void hideLog() {
		logVisible = false;
		body.remove(log);
		body.revalidate();
		body.repaint();
	}
	public void showNotification(String A) {
		if (notificationVisible) {
			body.remove(notification);
			body.revalidate();
		}
		notificationVisible = true;
		notification = new JPanel();
		notification.setSize(scaleSizeX * 8, scaleSizeY * 4);
		body.add(notification);
		body.setLayer(notification, 2);
		notification.setBackground(new Color(200, 100, 100));
		notification.setLocation(scaleSizeX * 4, scaleSizeY * 3);
		notification.setVisible(true);
		notification.setLayout(null);
		JLabel t = new JLabel("Notification!");
		t.setSize(scaleSizeX * 7, scaleSizeY);
		notification.add(t);
		t.setBackground(new Color(100, 200, 100));
		t.setLocation(scaleSizeX, 0);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX*2/3));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		JLabel e = new JLabel(A);
		e.setSize(scaleSizeX * 8, scaleSizeY * 3);
		notification.add(e);
		e.setBackground(new Color(100, 200, 100));
		e.setLocation(0, 0);
		e.setFont(new Font("Arial", Font.BOLD, scaleSizeX / 4));
		e.setVisible(true);
		JButton c = new JButton("X"); // Close notification window
		c.setSize(scaleSizeX, scaleSizeY);
		notification.add(c);
		c.setLocation(scaleSizeX * 7, 0);
		c.setBackground(Color.GREEN);
		c.setFont(new Font("Arial", Font.PLAIN, scaleSizeX / 2));
		c.setForeground(Color.WHITE);
		c.setVisible(true);
		c.addActionListener(this);
		c.setActionCommand(0 + "NO");
	}
	public void graphASelect(){
		JPanel graphSelectPanel = new JPanel();
		graphSelectPanel.setSize(scaleSizeX * 31 / 8, scaleSizeY * 17 / 8);
		body.add(graphSelectPanel);
		body.setLayer(graphSelectPanel, 2);
		graphSelectPanel.setLocation(scaleSizeX / 8, scaleSizeY * 110 / 16);
		graphSelectPanel.setBackground(new Color(200,200,50));
		graphSelectPanel.setVisible(true);
		graphSelectPanel.setLayout(null);
		JLabel t = new JLabel("Select Graph to Display");
		t.setSize(scaleSizeX * 3, scaleSizeY / 2);
		graphSelectPanel.add(t);
		t.setLocation(scaleSizeX/3*2, scaleSizeY / 8);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		JLabel tID = new JLabel("Stock ID (0-255)");
		tID.setSize(scaleSizeX*2,scaleSizeY/2);
		graphSelectPanel.add(tID);
		tID.setLocation(scaleSizeX/4,scaleSizeY/2);
		tID.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		tID.setForeground(Color.WHITE);
		tID.setVisible(true);
		SpinnerModel m1 = new SpinnerNumberModel(0,0,255,1);
		chartID = new JSpinner(m1);
		chartID.setSize(scaleSizeX,scaleSizeY/4);
		graphSelectPanel.add(chartID);
		chartID.setLocation(scaleSizeX*5/2,scaleSizeY/3*2);
		chartID.setVisible(true);
		JLabel dayT = new JLabel("# of Trading Days (1-250)");
		dayT.setSize(scaleSizeX*2,scaleSizeY);
		graphSelectPanel.add(dayT);
		dayT.setLocation(scaleSizeX/4,scaleSizeY*2/3);
		dayT.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		dayT.setForeground(Color.WHITE);
		dayT.setVisible(true);
		SpinnerModel m2 = new SpinnerNumberModel(30,1,250,1);
		dayCount = new JSpinner(m2);
		dayCount.setSize(scaleSizeX,scaleSizeY/4);
		graphSelectPanel.add(dayCount);
		dayCount.setLocation(scaleSizeX*5/2,scaleSizeY);
		dayCount.setVisible(true);
		JButton graphUpdate = new JButton("Generate Graph");
		graphUpdate.setSize(scaleSizeX*3,scaleSizeY/2);
		graphSelectPanel.add(graphUpdate);
		graphUpdate.setLocation(scaleSizeX/4,scaleSizeY*3/2);
		graphUpdate.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		graphUpdate.setBackground(Color.BLUE);
		graphUpdate.setForeground(Color.WHITE);
		graphUpdate.addActionListener(this);
		graphUpdate.setActionCommand("9");
		graphUpdate.setVisible(true);
	}
	public void transferPanel(){
		JPanel tP = new JPanel();
		tP.setSize(scaleSizeX * 31 / 8, scaleSizeY * 17 / 8);
		body.add(tP);
		body.setLayer(tP, 2);
		tP.setLocation(scaleSizeX * 96 / 8, scaleSizeY * 155 / 16);
		tP.setBackground(new Color(100,200,50));
		tP.setVisible(true);
		tP.setLayout(null);
		JLabel t = new JLabel("Transfer Account Balance");
		t.setSize(scaleSizeX * 4, scaleSizeY / 2);
		tP.add(t);
		t.setLocation(scaleSizeX/4, scaleSizeY / 8);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		JLabel tSE = new JLabel("Bank to Stock");
		tSE.setSize(scaleSizeX*2,scaleSizeY/2);
		tP.add(tSE);
		tSE.setLocation(scaleSizeX/4,scaleSizeY/2);
		tSE.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		tSE.setForeground(Color.WHITE);
		tSE.setVisible(true);
		SpinnerModel m1 = new SpinnerNumberModel(0,0,Integer.MAX_VALUE,1);
		transferBank = new JSpinner(m1);
		transferBank.setSize(scaleSizeX,scaleSizeY/4);
		tP.add(transferBank);
		transferBank.setLocation(scaleSizeX/4,scaleSizeY);
		transferBank.setVisible(true);
		JLabel tBA = new JLabel("Stock to Bank");
		tBA.setSize(scaleSizeX*2,scaleSizeY);
		tP.add(tBA);
		tBA.setLocation(scaleSizeX*2,scaleSizeY/4);
		tBA.setFont(new Font("Arial", Font.BOLD, scaleSizeX/6));
		tBA.setForeground(Color.WHITE);
		tBA.setVisible(true);
		SpinnerModel m2 = new SpinnerNumberModel(0,0,Integer.MAX_VALUE,1);
		transferStock = new JSpinner(m2);
		transferStock.setSize(scaleSizeX,scaleSizeY/4);
		tP.add(transferStock);
		transferStock.setLocation(scaleSizeX*2,scaleSizeY);
		transferStock.setVisible(true);
		JButton transfer = new JButton("Transfer");
		transfer.setSize(scaleSizeX*3,scaleSizeY/2);
		tP.add(transfer);
		transfer.setLocation(scaleSizeX/4,scaleSizeY*3/2);
		transfer.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		transfer.setBackground(Color.BLUE);
		transfer.setForeground(Color.WHITE);
		transfer.addActionListener(this);
		transfer.setActionCommand("8");
		transfer.setVisible(true);
	}
	public void hideNotification() {
		notificationVisible = false;
		body.remove(notification);
		body.revalidate();
		body.repaint();
	}
	public void refreshBuySell() {
		buySell = new JPanel();
		buySell.setSize(scaleSizeX * 7 / 2, scaleSizeY * 7 / 2);
		body.add(buySell);
		body.setLayer(buySell, 2);
		buySell.setLocation(scaleSizeX * 97 / 8, scaleSizeY * 5 / 4);
		buySell.setBackground(Color.BLUE);
		buySell.setVisible(true);
		buySell.setLayout(null);
		JLabel t = new JLabel("Buy and Sell Stocks");
		t.setSize(scaleSizeX * 3, scaleSizeY / 2);
		buySell.add(t);
		t.setLocation(scaleSizeX*3/4, scaleSizeY / 8);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX/5));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		JLabel tID = new JLabel("Stock ID (0-255)");
		tID.setSize(scaleSizeX,scaleSizeY/4);
		buySell.add(tID);
		tID.setLocation(scaleSizeX/3,scaleSizeY*3/4);
		tID.setFont(new Font("Arial", Font.BOLD, scaleSizeX/8));
		tID.setForeground(Color.WHITE);
		tID.setVisible(true);
		SpinnerModel m1 = new SpinnerNumberModel(0,0,255,1);
		bSID = new JSpinner(m1);
		bSID.setSize(scaleSizeX,scaleSizeY/4);
		buySell.add(bSID);
		bSID.setLocation(scaleSizeX/4,scaleSizeY);
		bSID.setVisible(true);
		JLabel tQu = new JLabel("Quantity");
		tQu.setSize(scaleSizeX,scaleSizeY/4);
		buySell.add(tQu);
		tQu.setLocation(scaleSizeX*2,scaleSizeY*3/4);
		tQu.setFont(new Font("Arial", Font.BOLD, scaleSizeX/8));
		tQu.setForeground(Color.WHITE);
		tQu.setVisible(true);
		SpinnerModel m2 = new SpinnerNumberModel(0,0,Integer.MAX_VALUE,1);
		bSQu = new JSpinner(m2);
		bSQu.setSize(scaleSizeX,scaleSizeY/4);
		buySell.add(bSQu);
		bSQu.setLocation(scaleSizeX*2,scaleSizeY);
		bSQu.setVisible(true);
		bSTo = new JLabel("$"+(dS.getStockArray()[(Integer)bSID.getValue()].getPrice()*(Integer)bSQu.getValue()));
		bSTo.setSize(scaleSizeX*5/2,scaleSizeY/2);
		buySell.add(bSTo);
		bSTo.setLocation(scaleSizeX,scaleSizeY*5/4);
		bSTo.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		bSTo.setForeground(Color.GREEN);
		bSTo.setVisible(true);
		JButton bSBuy = new JButton("Buy");
		bSBuy.setSize(scaleSizeX*3,scaleSizeY*3/4);
		buySell.add(bSBuy);
		bSBuy.setLocation(scaleSizeX/4,scaleSizeY*7/4);
		bSBuy.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		bSBuy.setBackground(Color.GREEN);
		bSBuy.setForeground(Color.BLACK);
		bSBuy.addActionListener(this);
		bSBuy.setActionCommand("1");
		JButton bSSell = new JButton("Sell");
		bSSell.setSize(scaleSizeX*3,scaleSizeY*3/4);
		buySell.add(bSSell);
		bSSell.setLocation(scaleSizeX/4,scaleSizeY*5/2);
		bSSell.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		bSSell.setBackground(Color.RED);
		bSSell.setForeground(Color.BLACK);
		bSSell.addActionListener(this);
		bSSell.setActionCommand("2");
		buySell.revalidate();
		buySell.repaint();
	}
	public void updateBuySellTotal(){
		bSTo.setText("$"+(dS.getStockArray()[(Integer)bSID.getValue()].getPrice()*(Integer)bSQu.getValue()));
		buySell.revalidate();
		buySell.repaint();
	}
	public void refreshPortfolio(){
		if (portfolioInit){
			body.remove(portfolio);
		}
		portfolioInit = true;
		portfolio = new JPanel();
		portfolio.setSize(scaleSizeX * 7 / 2, scaleSizeY * 9 / 2);
		body.add(portfolio);
		body.setLayer(portfolio, 2);
		portfolio.setLocation(scaleSizeX * 97 / 8, scaleSizeY * 20 / 4);
		portfolio.setBackground(new Color(200,100,50));
		portfolio.setVisible(true);
		portfolio.setLayout(null);
		JLabel t = new JLabel("Portfolio");
		t.setSize(scaleSizeX * 3, scaleSizeY / 2);
		portfolio.add(t);
		t.setLocation(scaleSizeX*5/4, scaleSizeY / 8);
		t.setFont(new Font("Arial", Font.BOLD, scaleSizeX/4));
		t.setForeground(Color.WHITE);
		t.setVisible(true);
		String[] col = new String[] {
			"ID",
			"Symbol",
			"Price",
			"Quantity"
			};
		Object[][] data = new Object[dS.getStockAccount().getStocksOwned().size()+3][4];
		for (int i = 0; i < dS.getStockAccount().getStocksOwned().size(); i++) {
			data[i][0] = "" + dS.getStockAccount().getStocksOwned().get(i).getStock().getID();
			data[i][1] = dS.getStockAccount().getStocksOwned().get(i).getStock().getSymbol();
			data[i][2] = "$"+dS.getStock(dS.getStockAccount().getStocksOwned().get(i).getStock().getID()).getPrice();
			data[i][3] = ""+dS.getStockAccount().getStocksOwned().get(i).getQuantity();
		}
		data[dS.getStockAccount().getStocksOwned().size()+1][0] = "Liquid";
		data[dS.getStockAccount().getStocksOwned().size()+1][1] = "$"+dS.getStockAccount().getBalance();
		data[dS.getStockAccount().getStocksOwned().size()+1][2] = "Bank";
		data[dS.getStockAccount().getStocksOwned().size()+1][3] = "$"+dS.getBankAccount().getBalance();
		data[dS.getStockAccount().getStocksOwned().size()+2][0] = "Stocks";
		data[dS.getStockAccount().getStocksOwned().size()+2][1] = "$"+(dS.getStockAccount().genTotalValue()-dS.getStockAccount().getBalance());
		data[dS.getStockAccount().getStocksOwned().size()+2][2] = "Total";
		data[dS.getStockAccount().getStocksOwned().size()+2][3] = "$"+dS.getTotalValue();
		DefaultTableModel model = new DefaultTableModel(data, col);
		stockTable = new JTable(model);
		stockTable.setSize(scaleSizeX * 12, scaleSizeY * 8);
		stockTable.setRowHeight(scaleSizeY / 4);
		stockTable.setFont(new Font("Arial", Font.PLAIN, scaleSizeX / 6));
		stockTable.setBackground(Color.BLACK);
		stockTable.setForeground(Color.WHITE);
		JScrollPane portScroll = new JScrollPane(stockTable);
		portScroll.setSize(scaleSizeX*7/2,scaleSizeY*4);
		portfolio.add(portScroll);
		portScroll.setLocation(0,scaleSizeY*3/4);
		stockTable.setVisible(true);
	}
	public String JLabelMultiLine(String A){
		return "<html>" + A.replaceAll("\n", "<br>")+"</html>";
	}
}