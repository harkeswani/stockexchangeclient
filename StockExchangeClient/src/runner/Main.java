package runner;
import graphics.*;
import update.*;
import java.awt.GraphicsDevice;
import java.awt.MouseInfo;

import data.LocalDataStore;
import data.*;
public class Main {
	public static void main(String args[]){
		GraphicsDevice gd = MouseInfo.getPointerInfo().getDevice();
		int xDim = gd.getDisplayMode().getWidth();
		int yDim = gd.getDisplayMode().getHeight();
		if (xDim>1920){
			xDim = 1920;
			yDim = 1080;
		}
		LocalDataStore dS = new LocalDataStore();
		dS.initStocks();
		dS.initStockAccount();
		dS.initBankAccount(10000);
		Display d = new Display(xDim, yDim);
		dS = new LocalDataStore(d);
		d.load();
		GUIUpdate uP = new GUIUpdate(d);
	}
}