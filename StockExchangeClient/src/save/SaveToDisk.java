package save;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import data.LocalDataStore;

public class SaveToDisk {
	LocalDataStore dS;
	public SaveToDisk(){
		dS = new LocalDataStore();
	}
	public void savePortfolio(){
		try{
		    PrintWriter writer = new PrintWriter("portfolio.txt", "UTF-8");
		    writer.print(dS.getBankAccount().getBalance()+",");
		    writer.print(dS.getStockAccount().getBalance()+",");
		    for (int i=0;i<dS.getStockAccount().getStocksOwned().size();i++){
		    	writer.print(dS.getStockAccount().getStocksOwned().get(i).getStock().getID()+",");
		    	writer.print(dS.getStockAccount().getStocksOwned().get(i).getQuantity()+",");
		    }
		    writer.close();
		} catch (IOException e) {
		   dS.getDisplay().showNotification("Failed to save to portfolio.");
		}
	}
	public void loadPortfolio(){
		try {
			BufferedReader br = new BufferedReader(new FileReader("portfolio.txt"));
			String result = "";
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				result+=sCurrentLine;
			}
			String[] r = result.split(",");
			dS.getBankAccount().setBalance(Double.parseDouble(r[0]));
			dS.getStockAccount().setBalance(Double.parseDouble(r[1]));
			for (int i=2;i<r.length;i+=2){
				dS.getStockAccount().addStock(dS.getStock(Integer.parseInt(r[i])),Integer.parseInt(r[i+1]));
			}
			dS.getDisplay().refreshPortfolio();
		} catch (Exception E){
			dS.getDisplay().showNotification("Error loading portfolio.");
		}
	}
	public void saveToLog(String A){
		try(FileWriter fw = new FileWriter("log.txt", true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
			{
			    out.println(A);
			} catch (IOException e) {
			    System.out.println("Logging failed.");
			}
	}
	public String getLog(){
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader("log.txt"));
			result = "";
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				result+=sCurrentLine+"\n";
			}
			return result;
		} catch (Exception E){
			return result;
		}
	}
}
